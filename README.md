# Speedtestcustom

Native Javascript Download Speed Test.

## Deploy

This project is designed to fit in an existing nginx htdocs directory.

```
cd /not/var/www/htdocs
git clone https://gitlab.com/hugostephan/speedtestnative
cp speedtestnative/* /var/www/htdocs/ -rf # not tested
```
