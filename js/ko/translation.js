(function() {
  translationData = {
    dt: {
      fr: {
        processing:     "Traitement en cours...",
        search:         "Rechercher&nbsp;:",
        lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        paginate: {
            first:      "Premier",
            previous:   "Pr&eacute;c&eacute;dent",
            next:       "Suivant",
            last:       "Dernier"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      en: {
        processing:     "Processing...",
        search:         "Search:",
        lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        paginate: {
            first:      "Premier",
            previous:   "Pr&eacute;c&eacute;dent",
            next:       "Suivant",
            last:       "Dernier"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      }
    },
    legendSelectTF: {
      fr: 'Choisissez l\'intervalle de temps à afficher',
      en: 'Select the time frame to be shown'
    },
    legendSelectTFSeconds: {
      fr: 'Choisissez l\'intervalle de temps à afficher (secondes)',
      en: 'Select the time frame to be shown (seconds)'
    },
    bandwithGraph: {
      fr: "Bande passante (Bps)",
      en: "Bandwith (Bps)"
    },
    timeGraph: {
      fr: "Temps",
      en: "Time"
    },
    current: {
      fr: "Heures",
      en: "Hours"
    },
    day: {
      fr: "Jour",
      en: "Day"
    },
    week: {
      fr: "Semaine",
      en: "Week"
    },
    month: {
      fr: "Mois",
      en: "Month"
    },
    year: {
      fr: "Année",
      en: "Year"
    },
    loading: {
      fr: "Chargement",
      en: "Loading"
    },
    inLegend: {
      fr: "Download",
      en: "Download"
    },
    outLegend: {
      fr: "Upload",
      en: "Upload"
    },
    globalStatus: {
      fr: "Statut rapide",
      en: "Quick status"
    },
    hardware: {
      fr: "Matériel",
      en: "Hardware"
    },
    gateway: {
      fr: "Passerelle",
      en: "Gateway"
    },
    connection: {
      fr: "Connexion",
      en: "Connection"
    },
    connectivity: {
      fr: "Connectivité",
      en: "Connectivity"
    },
    "interface": {
      fr: "Citybox",
      en: "Citybox"
    },
    latencyPhrase: {
      fr: "La latence entre votre adresse public et le Digital Manager est&nbsp: ",
      en: "The latency between your public address and the Digital Manager is: ",
      "fr_*": "La latence entre votre adresse public et le Digital Manager est&nbsp: @0@",
      "en_*": "The latency between your public address and the Digital Manager is: @0@"
    },
    citybsdVersion: {
      fr: "Version actuelle de citybsd&nbsp: ",
      en: "Current citybsd version: ",
      "fr_*": "Version actuelle de citybsd&nbsp: @0@",
      "en_*": "Current citybsd version: @0@"
    },
    configVersion: {
      fr: "Version de la configuration actuelle&nbsp: ",
      en: "Current config version: ",
      "fr_*": "Version de la configuration actuelle&nbsp: @0@",
      "en_*": "Current config version: @0@"
    },
    versionInfoPending: {
      fr: "La version courante est en cours de validation.",
      en: "The current version is pending.",
      "fr_*": "La version courante @0@ est en cours de validation.",
      "en_*": "The current version @0@ is pending."
    },
    versionInfofailed: {
      fr: "La derniere version n'est pas valide.",
      en: "The last versoin is not valid.",
      "fr_*": "La derniere version &lt;@0@&gt; n'est pas valide.",
      "en_*": "The last versoin &lt;@0@&gt; is not valid."
    },
    serialNumber: {
      fr: "Numéro de série&nbsp: ",
      en: "Serial number: ",
      "fr_*": "Numéro de série&nbsp: @0@",
      "en_*": "Serial number: @0@"
    },
    start: {
      fr:"Début",
      en: "Start"
    },
    now: {
      fr: "Maintenant",
      en: "Now"
    },
    temperature: {
      fr: "Température",
      en: "Temperature"
    },
    memory: {
      fr: "Mémoire",
      en: "Memory"
    },
    degree: {
      'fr': "C°",
      'en': "Celsius degree"
    },
    loads: {
      fr: "Charge Digital Manager",
      en: "Loads"
    },
    ethernetConnectionStatus: {
      fr: "Statut de la connexion ethernet",
      en: "Ethernet connection status"
    },
    status: {
      fr: "Statut",
      en: "Status"
    },
    address: {
      fr: "Adresse",
      en: "Address"
    },
    "access_point": {
      fr: "Point d'accès",
      en: "Access Point"
    },
    expire: {
      fr: "Expiration",
      en: "Expire"
    },
    noDefaultRoute: {
      fr: "Pas de route par défaut",
      en: "No default route"
    },
    failedReadingData: {
      fr: "Impossible de lire les données",
      en: "Failed to read any data"
    },
    advancedSupervision: {
      fr: "Supervision avancée",
      en: "Advanced supervision"
    },
    wanUsageSupervision: {
      fr: "Utilisation du réseau étendu",
      en: "Wide area network usage"
    },
    "full_connectivity": {
      fr: "VPN City",
      en: "City VPN"
    },
    "web": {
      fr: "Internet",
      en: "Internet"
    },
    "gw_only": {
      fr: "Passerelle",
      en: "Gateway"
    },
    "no_gw": {
      fr: "Pas de passerelle",
      en: "No gateway"
    },
    "no_interface": {
      fr: "Pas d'interface",
      en: "No interface"
    },
    off: {
      fr: "Éteint",
      en: "Shutdown"
    },
    backup: {
      fr: "Redondance",
      en: "Backup"
    },
    noCloudData: {
      fr: "Aucune donnée du cloud",
      en: "No cloud data"
    },
    emptyCloudData: {
      fr: "Données du cloud vides",
      en: "Empty cloud data"
    },
    uptimeHistoryChart: {
      fr: "Graphique des disponibilités",
      en: "Uptime history chart"
    },
    blockedPackets: {
      fr: "Paquets bloqués",
      en: "Blocked Packets"
    },
    advancedMonitoringGraph: {
      fr: "Monitoring avancé",
      en: "Advanced monitoring"
    },
    internetConnectionQuality: {
      fr: "Qualité de la connexion internet",
      en: "Internet connection quality"
    },
    stability: {
      fr: "Stabilité",
      en: "Stability"
    },
    axisTime: {
      fr: "Temps",
      en: "Time"
    },
    axisLatency: {
      fr: "Latence (ms)",
      en: "Latency (ms)"
    },
    axisBandwidthb: {
      fr: "Bande passante (bps)",
      en: "Bandwidth (bps)"
    },
    dns: {
      fr: "Résolution de domaine",
      en: "Dns"
    },
    gw: {
      fr: "Passerelle",
      en: "Gateway"
    },
    bandwidth: {
      fr: "Bande passante",
      en: "Bandwidth"
    },
    wifiManagement: {
      fr: "Gestion wifi",
      en: "Manage wifi"
    },
    administration: {
      fr: "Administration",
      en: "Administration"
    },
    wiwiAccessPoint: {
      fr: "Points d'accès wifi",
      en: "Wifi Access Points"
    },
    wifiClients: {
      fr: "Clients sur les points d'accès wifi",
      en: "Clients using the Wifi Access Points"
    },
    wifiNetworkMap: {
      fr: "Cartographie Reseaux Wifi",
      en: "Wifi Network Map"
    },
    wifiCoverageGraph: {
      fr: "Graphique de la couverture radio",
      en: "Radio coverage graphic"
    },
    download: {
      fr: "Téléchargement",
      en: "Download"
    },
    downloadUnit: {
      fr: "Mo/s",
      en: "Mbps"
    },
    downloadLogsTitle: {
      fr: "Téléchargement des logs du boitier",
      en: "Download logs"
    },
    downloadLogs: {
      fr: "Télécharger les logs",
      en: "Download logs"
    },
    sendLogByMailHelp: {
      fr: "Télécharger les logs",
      en: "Download logs"
    },
    sendLogsToSupport: {
      fr: "Envoyer au support",
      en: "Send to support"
    },
    fitZoom: {
      fr: "Adapter",
      en: "Fit zoom"
    },
    resetZoom: {
      fr: "Réinitialiser",
      en: "reset zoom"
    },
    displayName: {
      fr: "Nom du wifuseur",
      en: "Wifuseur name"
    },
    seeCity: {
      fr: "Wifuseur(s) vu(s)",
      en: "Wifuseur(s) seen"
    },
    seenBy: {
      fr: "Vu par",
      en: "Seen by"
    },
    seenAt: {
      fr: "Vu le",
      en: "Seen at"
    },
    goodReception: {
      fr: "Bruit fort",
      en: "Strong noise"
    },
    mediumReception: {
      fr: "Bruit medium",
      en: "Medium noise"
    },
    badReception: {
      fr: "Bruit faible",
      en: "Weak noise"
    },
    ssid: {
      fr: "SSID",
      en: "SSID"
    },
    wifuseurName: {
      fr: "Nom wifuseur",
      en: "Wifuseur name"
    },
    clientHostname: {
      fr: "Hostname client",
      en: "Client hostname"
    },
    clientMac: {
      fr: "Mac du client",
      en: "Client mac"
    },
    clientIP: {
      fr: "Client adresse IP",
      en: "Client IP address"
    },
    clientPower: {
      fr: "Signal du client en dBm",
      en: "Client signal in dBm"
    },
    bytes: {
      fr: "octets",
      en: "bytes"
    },
    FailedLoadNetworkInfo: {
      fr: "Echec du chargement des informations du réseau",
      en: "Failed loading network information"
    },
    FailedLoadStates: {
      fr: "Echec du chargement des états",
      en: "Failed loading states"
    },
    notEnoughDataToDrawGraph: {
      fr: "Pas assez de donnée disponible pour afficher ces informations",
      en: "Not enough data to display this information"
    },
    sunburstObjectOutWan: {
      fr: "Détails des données uploadées pour chaque objet vers internet",
      en: "Detailed upload in bytes per object to internet"
    },
    sunburstObjectInWan: {
      fr: "Détails des données téléchargées pour chaque objet depuis internet",
      en: "Detailed download in bytes per object from internet"
    },
    sunburstObjectOutVpn: {
      fr: "Détails des données uploadées pour chaque objet vers le VPN",
      en: "Detailed upload in bytes per object to VPN"
    },
    sunburstObjectInVpn: {
      fr: "Détails des données téléchargées pour chaque objet depuis le VPN",
      en: "Detailed download in bytes per object from VPN"
    },
    sunburstObjectOutLan: {
      fr: "Détails des données uploadées pour chaque objet vers le réseau local",
      en: "Detailed upload in bytes per object to local network"
    },
    sunburstObjectInLan: {
      fr: "Détails des données téléchargées pour chaque objet depuis le réseau local",
      en: "Detailed download in bytes per object from local network"
    },
    sunburstObjectDescription: {
      fr: "Capture le détail de l'utilisation du réseau sur une courte période de temps pour détecter une utilisation anormale ou signaler l'utilisation quotidienne classique de la bande passante.",
      en: "Capture the detail of network usage over a short period of time to detect abnormal usage or report classic daily usage of the bandwtih(s)."
    },
    sunburst: {
      fr: "Utilisation du réseau",
      en: "Network usage"
    },
    startObjectUsageCapture: {
      fr: "Démarrer la capture",
      en: "Start the capture"
    },
    stopObjectUsageCapture: {
      fr: "Arrêter la capture",
      en: "Stop the capture"
    },
    date: {
      fr: "Date",
      en: "Date"
    },
    source: {
      fr: "Source",
      en: "Source"
    },
    destination: {
      fr: "Destination",
      en: "Destination"
    },
    rule: {
      fr: "Règle",
      en: "Rule"
    },
    info: {
      fr: "Information",
      en: "Information"
    },
    logsMailBody: {
      fr: "Veuillez expliquer votre problème et mettre en pièce jointe le fichier de log téléchargé. Le nom du fichier est de la forme citylogs-$lt;la date actuelle&gt;.txt.",
      en: "Please explain your problem, and attach the downloaded log file to this mail. The file's name will be citylogs-&lt;current date&gt;.txt."
    },
    show: {
      fr: "Afficher",
      en: "Show"
    },
    hide: {
      fr: "Cacher",
      en: "Hide"
    },
    refresh: {
      fr: "Rafraîchir",
      en: "Refresh"
    },
    supervision: {
      fr: "Supervision",
      en: "Supervision"
    },
    networkLayout: {
      fr: "Liste des équipements du réseau local",
      en: "Local network's devices list"
    },
    startSpeedtest: {
      fr: "Tester la connexion",
      en: "Test connection"
    },
    speedtestLayout: {
      fr: "Vitesse de connexion",
      en: "Connection's speed"
    },
    networkName: {
      fr: "Réseau",
      en: "Network"
    },
    macAddress: {
      fr: "Adresse mac",
      en: "Mac address"
    },
    objectName: {
      fr: "Nom",
      en: "Name"
    },
    vlan: {
      fr: "vlan",
      en: "vlan"
    },
    port: {
      fr: "Port",
      en: "Port"
    },
    unfiltered: {
      fr: "Non filtré",
      en: "Unfiltered"
    },
    clients_title: {
      fr: "Appareils clients",
      en: "Client devices"
    },
    ip : {
      fr: "Adresse IP",
      en : "IP address"
    },
    hostname : {
      fr: "Nom d'hôte",
      en : "Host Name"
    },
    action: {
      fr: "Action",
      en: "Action"
    },
    password: {
      fr: "Mot de passe",
      en: "Password"
    },
    macDetail: {
      fr: "Détail MAC",
      en: "MAC detail"
    },
    hiddenDate: {
      fr: "Timestamp",
      en: "Timestamp"
    },
    startDate: {
      fr: "Début de la connexion",
      en: "Connection start date"
    },
    duration: {
      fr: "Durée",
      en: "Duration"
    },
    site: {
      fr: "Site",
      en: "Site"
    },
    identifier: {
      fr: "Identifiant",
      en: "Identifier"
    },
    blacklistTitle: {
       fr: "Appareils en liste noire",
       en: "Blacklisted devices"
     },
     addBlacklist: {
       fr: "Ajouter un appareil à la liste noire",
       en: "Blacklist a device",
     },
     saving: {
      fr: "Sauvegarde",
      en: "Saving"
     },
     name: {
      fr: "Nom",
      en: "Name"
     },
     banReason: {
      fr: "Raison du bannissement",
      en: "Banishment reason"
    },
    blacklistMac: {
      fr: "Adresse MAC à bannir",
      en: "Mac address to blacklist"
    },
    blacklistName: {
      fr: "Nom",
      en: "Name"
    },
    blacklistReason: {
      fr: "Raison du bannisement",
      en: "Reason"
    },
    submitBlacklist: {
      fr: "Valider",
      en: "Submit"
    },
    errorCode: {
      fr: "Code d'erreur",
      en: "Error code"
    },
    serverError: {
      fr: "Erreur du server",
      en: "Server error"
    },
    unknowError: {
      fr: "Erreur inconnue",
      en: "Unknown error"
    },
    error: {
      fr: "Erreur",
      en: "Error"
    },
    connected: {
      fr: "Connecté",
      en: "Connected"
    },
    disconnected: {
      fr: "Déconnecté",
      en: "Disconnected"
    },
    authorize: {
      fr: "Autoriser",
      en: "Authorize"
    },
    defineNameUser: {
      fr: "Définir un nom d'utilisateur",
      en: "Define user name"
    },
    kick: {
      fr: "Expulser",
      en: "Kick"
    },
    blacklist: {
      fr: "Bannir",
      en: "Blacklist"
    },
    noPassword: {
      fr: "Aucun mot de passe",
      en: "No password"
    },
    blacklistSaved: {
      fr: "Liste noire sauvegardée",
      en: "Blacklist saved"
    },
    macUnblacklisted: {
      fr: "Adresse MAC retirée de la liste noire",
      en: "MAC address unblacklisted"
    },
    removeFromBlacklist: {
      fr: "Retirer de la liste noire",
      en: "Remove from blacklist"
    },
    citypassengerAdviceNotUseThis: {
      fr: "Citypassenger conseille de ne pas utiliser ces boutons sauf sur demande du support technique",
      en: "Citypassenger advise is to not use this pannel, except when technical support asks"
    },
    restartAndUpgrade: {
      fr: "Redémarrer et mettre à jour",
      en: "Restart and upgrade"
    },
    toUseOnSupportAsk: {
      fr: "N'utiliser qu'à la demande du support",
      en: "Only use when asked to by technical support"
    },
    updateProgress: {
      fr: "Avancement de la mise à jour",
      en: "Update progress"
    },
    reboot: {
      fr: "Redémarrer",
      en: "Reboot"
    },
    rebooting: {
      fr: "Redémarrage",
      en: "Rebooting"
    },
    updateRouter: {
      fr: "Mettre à jour le boitier",
      en: "Update device"
    },
    updateRouterHelp: {
      fr: "Mettre à jour le système d'exploitation du boitier. Nécessitera un redémarrage.",
      en: "Update the device's operating system. Will require a reboot."
    },
    update: {
      fr: "Mettre à jour",
      en: "Update"
    },
    rebootRouter: {
      fr: "Redémarrer le boitier",
      en: "Reboot device"
    },
    rebootRouterHelp: {
      fr: "Redémarre le boitier. Il lui faudra quelques minutes pour recharger la configuration active.",
      en: "Reboots the device. It will need a few minutes to reload the active configuration."
    },
    rebootRouterMaintenance: {
      fr: "Redémarrer en mode maintenance",
      en: "Reboot in maintenance mode"
    },
    rebootRouterMaintenanceHelp: {
      fr: "Redémarre et entre en mode maintenance. Ne pas utiliser sauf en cas de demande explicite du support technique de CityPassenger.",
      en: "Reboots in maintenance mode. This feature must only be used if specifically requested by a member of the Citypassenger Technical Support team."
    },
    rebootForMaintenance: {
      fr: "Mode maintenance",
      en: "Maintenance mode"
    },
    UpgradeBinaries: {
      fr: "Mettre à jour les packages",
      en: "Update the packages"
    },
    UpgradeBinariesHelp: {
      fr: "Met à jour l'ensemble des packages du boitier. Les modifications persisteront après les redémarrages. N'utiliser qu'en cas de demande explicite du support technique CityPassenger.",
      en: "Update the device's packages. Changes will persist after reboots. This feature must only be used if specifically requested by a member of the Citypassenger Technical Support team."
    },
    Go: {
      fr: "Mettre à jour",
      en: "Update"
    },
    logs: {
      fr: "Logs",
      en: "Logs"
    },
    logsHelp: {
      fr: "TODO: logsHelp",
      en: "TODO: logsHelp"
    },
    notLogged: {
      fr: "Vous n'êtes pas authentifié",
      en: "Not logged"
    },
    subnetActiveLease: {
      fr: "Baux actifs",
      en: "Active lease"
    },
    subnetInactiveLease: {
      fr: "Baux inactifs",
      en: "Inactive lease"
    },
    "localFileEditor.select.error": {
      fr: "Impossible d'ouvrir ce fichier",
      en: "Couldn't open this file"
    },
    "localFileEditor.title": {
      fr: "Édition de fichiers locaux",
      en: "Local files editor"
    },
    "localFileEditor.help": {
      fr: "En cas de problème de connexion internet vous pouvez modifier la configuration wan localement ici. Ces modifications sont temporaires et seront perdues au redémarrage du Cloudgate. Pour rendre ces modifications permanentes il faut mettre à jour les fichiers dans l'interface Cityscope. Créer un tag. Attendre que le Cloudgate passe à cette version. Puis supprimer les fichiers créés ici dans les 5 minutes suivant la récupération de la nouvelle version pour valider le tag créé sur Cityscope.",
      en: "In case of WAN configuration issues you to locally edit this Cloudgate's files. These modifications are temporary and will be lost upon the Cloudgate's reboot. To make these modifications permanent, you need to edit the files on the Cityscope interface. Create a tag. Wait for the Cloudgate to get this version. And then, delete the local files you edited here within 5 minute of getting the new version to allow the Cloudgate to validate the tag"
    },
    "localFileEditor.edit.success": {
      fr: "Fichier édité avec succès",
      en: "File successfully edited"
    },
    "localFileEditor.edit": {
      fr: "Sauvegarder les changements",
      en: "Save changes"
    },
    "localFileEditor.edit.error": {
      fr: "Erreur lors de l'édition du fichier",
      en: "Error while editing"
    },
    "localFilesFound": {
      fr: "Des fichiers ont été modifiés localement. Ces modifications seront perdues au redémarrage.",
      en: "Files were locally edited. The modifications will be lost upon reboot."
    },
    "localFileEditor.selectFile": {
      fr: "Choisir un fichier à éditer",
      en: "Choose a file to edit"
    },
    "localFileEditor.editor": {
      fr: "Éditeur de fichier",
      en: "File editor"
    },
    "changedFiles": {
      fr: "Fichiers modifiés localement",
      en: "Locally edited files"
    },
    "localFileEditor.delete.all": {
      fr: "Supprimer toutes les modifications locales",
      en: "Delete all local changes"
    },
    subnetTotalDhcpRange: {
      fr: "Plage DHCP inutilisée",
      en: "Unused DHCP range"
    },
    subnetRange: {
      fr: "Sous réseau inutilisé",
      en: "Free subnet range"
    },
    subnetPirate: {
      fr: "Objets non déclarés",
      en: "Unspecified objects"
    },
    subnetObjects: {
      fr: "Objets",
      en: "Objects"
    },
    ipAdressesNumber: {
      fr: "Nombre d'adresses IP",
      en: "Number of IP address"
    },
    subnetNoDhcp: {
      fr: "Aucun DHCP",
      en: "No DHCP"
    },
    subnetUnlawful: {
      fr: "Mal configurés",
      en: "Unlawful"
    },
    lanOccupancy: {
      fr: "Occupation des lans",
      en: "Lans occupancy"
    },
    "sensors.km0.temp0" : {
      "fr_*_*_*" : "Température CPU : @0@ @2@",
      "en_*_*_*" : "CPU temperature: @0@ @2@",
      "fr_*_*_degC" : "Température CPU : @0@°C",
      "en_*_*_degC" : "CPU temperature: @0@°C",
      fr : "Température CPU : @0@ @2@",
      en : "CPU temperature: @0@ @2@"
    },
      "sensors.it0.temp0" : {
      "fr_*_*_*" : "",
      "en_*_*_*" : "",
      "fr_*_*_degC" : "",
      "en_*_*_degC" : "",
      fr : "",
      en : ""
    },
      "sensors.it0.temp1" : {
      "fr_*_*_*" : "",
      "en_*_*_*" : "",
      "fr_*_*_degC" : "",
      "en_*_*_degC" : "",
      fr : "",
      en : ""
    },
      "sensors.it0.temp2" : {
      "fr_*_*_*" : "",
      "en_*_*_*" : "",
      "fr_*_*_degC" : "",
      "en_*_*_degC" : "",
      fr : "",
      en : ""
    },
    "sensors.it0.volt0" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt1" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt2" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt3" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt4" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt5" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt6" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt7" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.it0.volt8" : {
      "fr_*_*_*" : "Voltage @1@ : @0@",
      "en_*_*_*" : "Voltage @1@ : @0@",
      fr : "Voltage @1@ : @0@",
      en : "Voltage @1@ : @0@"
    },
    "sensors.cpu0.temp0" : {
      "fr_*_*_*" : "Température CPU : @0@ @2@",
      "en_*_*_*" : "CPU temperature: @0@ @2@",
      "fr_*_*_degC" : "Température CPU : @0@°C",
      "en_*_*_degC" : "CPU temperature: @0@°C",
      fr : "Température CPU : @0@ @2@",
      en : "CPU temperature: @0@ @2@"
    },
    "sensors.acpitz0.temp0" : {
      "fr_*_*_*" : "Température de l'alimentation : @0@",
      "en_*_*_*" : "Power supply temperature: @0@",
      fr : "Température de l'alimentation : @0@",
      en : "Power supply temperature: @0@"
    },
    "sensors.upd0.indicator0" :
    {
      "en_On _*_*" : "UPS: Charging",
      "en_Off _*_OK" : "UPS: Not charging",
      "en_Off _*_*" : "UPS: 🚨 Not charging 🚨",
      "fr_On _*_*" : "APC : En charge",
      "fr_Off _*_OK" : "APC : Ne se charge pas",
      "fr_Off _*_*" : "APC : 🚨 Ne se charge pas 🚨",
      fr : "APC : En charge @0@",
      en : "UPS: Charging @0@"
    },
    "sensors.upd0.indicator1" :
    {
      "en_Off _*_*" : "UPS: Not discharging",
      "en_On _*_OK" : "UPS: Discharging",
      "en_On _*_*" : "UPS: 🚨 Discharging 🚨",
      "fr_Off _*_*" : "APC : Ne se décharge pas",
      "fr_On _*_OK" : "APC : En décharge ?",
      "fr_On _*_*" : "APC : 🚨 En décharge 🚨",
      fr : "APC : En charge @0@",
      en : "UPS: Discharging @0@"
    },
    "sensors.upd0.indicator2" :
    {
      "en_On _ACPresent_*" : "UPS: AC supply OK",
      "en_Off _ACPresent_OK" : "UPS: AC supply not OK ?",
      "en_Off _ACPresent_*" : "UPS: 🚨 No AC 🚨",
      "fr_On _ACPresent_*" : "APC : Arrivée de courant OK",
      "fr_Off _ACPresent_OK" : "APC : Pas de courant ?",
      "fr_Off _ACPresent_*" : "APC : 🚨 Pas de courant 🚨",
      fr : "APC : Arrivée de courant @0@",
      en : "UPS: AC power @0@"
    },
    "sensors.upd0.indicator3" : {
      "en_Off _*_*" : "UPS: No overload",
      "en_On _*_OK" : "UPS: Overload ?",
      "en_On _*_*" : "UPS: 🚨 Overload 🚨",
      "fr_Off _*_*" : "APC : Pas de surcharge sur l'APC",
      "fr_On _*_OK" : "APC : En surcharge ?",
      "fr_On _*_*" : "APC : 🚨 En surcharge 🚨",
      fr : "APC : Surcharge @1@",
      en : "APC: Overload @1@"
    },
    "sensors.upd0.percent0" :
    {
      "fr_*_*_OK" : "APC : Capacité de batterie restante @0@",
      "fr_*_*_NOK" : "APC : 🚨 Capacité de batterie restante @0@ 🚨",
      "en_*_*_OK" : "UPS: Remaining battery capacity @0@",
      "en_*_*_NOK" : "UPS: 🚨 Remaining battery capacity @0@ 🚨",
      fr : "APC : Capacité de batterie restante @0@",
      en : "UPS: Remaining battery capacity @0@"
    },
    "sensors.upd0.percent1" :
    {
      "fr_*_*_OK" : "APC : Capacité maximale de la batterie @0@",
      "fr_*_*_NOK" : "APC : 🚨 Capacité maximale de la batterie @0@ 🚨",
      "en_*_*_OK" : "UPS: Maximum battery capacity @0@",
      "en_*_*_NOK" : "UPS: 🚨 Maximum battery capacity @0@ 🚨",
      fr : "APC : Capacité maximale de la batterie @0@",
      en : "UPS: Maximum battery capacity @0@"
    },
    "sensors.upd0.timedelta0" :
    {
      "fr_*_*_OK" : "APC : Temps sur batterie @0@",
      "fr_*_*_NOK" : "APC : Attention Temps sur batterie @0@",
      "en_*_*_OK" : "Remaining battery time @0@",
      "en_*_*_NOK" : "Attention remaining battery time @0@",
      fr : "Temps sur battery @0@",
      en : "Run Time To Empty @0@"
    },
    "sensors.it0.fan0" : {
      "fr_*_*_*" : "",
      "en_*_*_*" : "",
      fr : "",
      en : ""
    },
    "sensors.it0.fan1" : {
      "fr_*_*_*" : "",
      "en_*_*_*" : "",
      fr : "",
      en : ""
    },
    "sensors.it0.fan2" : {
      "fr_*_*_*" : "",
      "en_*_*_*" : "",
      fr : "",
      en : ""
    },
    "Disks" :
    {
      fr : "Disques ",
      en : "Disks"
    }
    ,
    "Sensors" :
    {
      fr : "Sondes ",
      en : "Sensors"
    },
    "phone" :
    {
      fr : "Numéro de téléphone",
      en : "Phone number"
    },
    "errorInputPatternTooLong" :
    {
      fr : "Veuillez saisir moins de 64 caractères, les caractères spéciaux sont interdits sauf - ou _",
      en : "Please enter less than 64 characters, special characters are not allowed expect _ or -"
    },
    "errorInputPatternNumberOnly" :
    {
      fr : "Veuillez saisir seulement des chiffres",
      en : "Only numbers are allowed",
    },
    "hostnamePlaceholder" :
    {
      fr : "Saisir le nom d'hôte",
      en : "Enter the hostname",
    },
    "namePlaceholder" :
    {
      fr : "Saisir le nom",
      en : "Enter the name",
    },
    "phonePlaceholder" :
    {
      fr : "Saisir le numéro de téléphone",
      en : "Enter the phone number",
    },
    "moreDetails" :
    {
      fr : "Plus d'informations",
      en : "More details",
    },
    "authMethod" :
    {
      fr: "Méthode d'authentification",
      en: "Authentication method",
    },
    "identifiers_history" :
    {
      fr: "Identifiants clients",
      en: "Client identifier",
    },
    "on": {
      fr: "sur",
      en: "on"
    },
    "unidentified":
    {
      fr: "En cours d'authentification",
      en: "Authenticating"
    },
    "unspecified":
    {
      fr: "Non renseigné",
      en: "unspecified"
    },
    "host":
    {
      fr: "Hôte: ",
      en: "Host: "
    },
    "RescueSSH.title":
    {
      fr: "RescueSSH",
      en: "RescueSSH"
    },
    "RescueSSH.subtitle":
    {
      fr: "Démarre ou arrête RescueSSH",
      en: "Start or stop RescueSSh"
    },
    "RescueSSH.start":
    {
      fr: "Démarrer",
      en: "Start"
    },
    "RescueSSH.stop":
    {
      fr: "Arreter",
      en: "Stop"
    },
    "RescueSSH.start.success":
    {
      fr: "Rescuessh a été démarré",
      en: "Rescuessh has been started"
    },
    "RescueSSH.stop.success":
    {
      fr: "Rescuessh a été arrété",
      en: "Rescuessh has been stopped"
    },
    "radioScan.toggle.label":
    {
      fr: "Analyse radio active",
      en: "Perform constant scanning"
    }
  };

  window.translationData = translationData;
})();
