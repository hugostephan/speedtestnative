/*
	Speedtest HTML5/JS
	based on https://github.com/adolfintel/speedtest/
	GNU LGPLv3 License
*/

function Speedtest() {
  this.settings = {}; //settings for the speedtest worker
  this.state = 0; //0=adding settings, 1=adding servers, 2=server selection done, 3=test running, 4=done
}

Speedtest.prototype = {
  constructor: Speedtest,

  /**
   * Returns the state of the test: 0=adding settings, 1=adding servers, 2=server selection done, 3=test running, 4=done
   */
  getState: function () {
    return this.state;
  },

  /**
   * Starts the test.
   * During the test, the onupdate(data) callback function will be called periodically with data from the worker.
   * At the end of the test, the onend(aborted) function will be called with a boolean telling you if the test was aborted or if it ended normally.
   */
  start: function () {
    if (this.state == 3) throw "Test already running";
    this.worker = new Worker("./js/speedtest_worker.js?r=" + Math.random());
    this.worker.onmessage = function (e) {
      if (e.data === this._prevData) return;
      else this._prevData = e.data;
      var data = JSON.parse(e.data);
      try {
        if (this.onupdate) this.onupdate(data);
      } catch (e) {
        console.error("Speedtest onupdate event threw exception: " + e);
      }
      if (data.testState >= 4) {
        try {
          if (this.onend) this.onend(data.testState == 5);
        } catch (e) {
          console.error("Speedtest onend event threw exception: " + e);
        }
        clearInterval(this.updater);
        this._state = 4;
      }
    }.bind(this);
    this.updater = setInterval(
      function () {
        this.worker.postMessage("status");
      }.bind(this),
      200
    );

    if (this.state == 1 || this.state == 2)
      throw "Test is not ready to start, check configuration";

    this.state = 3;
    this.worker.postMessage("start " + JSON.stringify(this.settings));
  },

  /**
   * Aborts the test while it's running.
   */
  abort: function () {
    if (this.state < 3) throw "You cannot abort a test that's not started yet";
    if (this.state < 4) this.worker.postMessage("abort");
  }
};
